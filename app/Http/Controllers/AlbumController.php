<?php

namespace App\Http\Controllers;

use App\Clients\SpotifyClient;
use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\Token;
use GuzzleHttp\Client;

class AlbumController extends Controller
{
    private $spotifyClient;

   /**
     *Metodo que permite obtener la discografia de un artista
     *
     * @param  string  $artistName
     * @return Response
     */
    public function getAlbums(Request $request){
        #Creo un client para las peticiones a la API Spotify
            $artistName=$request->get('q');
            if(!isset($artistName)){
                return response()->json([
                    'error' => [
                        'code' => 'error',
                        'message' => 'Favor de incluir el nombre de la banda que en la URL.',
                    ]
                ], 400);
            }
            $this->spotifyClient = new SpotifyClient();
            $coincidencia=false;
        #Buscar el token en la BD
            $savedToken=Token::orderBy('id', 'DESC')->first();
    
            if(isset($savedToken)){
                $access_token=$savedToken->access_token;
            }else{
                #Solicitud de access_token a la API de Spotify
                $access_token = $this->requestAccessToken();
                if(isset($access_token->error)){
                    return response()->json($access_token->error);
                }
                
            }
        
        #Hacer solicitud a Spoti API para traer id de artista
            $artistas=$this->requestArtist($artistName,$access_token);
        
            if($artistas->getStatusCode() == 401){
                #Solicito y guardo NUEVAMENTE un access_token
                $access_token = $this->requestAccessToken();
                if(isset($access_token->error)){
                    return response()->json($access_token->error);
                }
                $artistas=$this->requestArtist($artistName,$access_token);
            }
            if($artistas->getStatusCode() == 200){
                $artistas=json_decode($artistas->getBody());
            }else{
                return response()->json([
                    'error' => [
                        'code' => 'error',
                        'message' => json_decode($artistas->getBody())
                    ]
                ], 500); 
            }

            $albumes=array();
            
            foreach($artistas->artists->items as $artista){
                #La busqueda por nombre de artista trae muuchas ocurrencias. Por tanto valido que el nombre ingresado sea exacto al que me trae Spotify API
                if(strtolower($artista->name) === strtolower(urldecode($artistName))){
                    
                    $artistId=$artista->id;
                    #Hacer solicitud a Spoti API para traer discografia
                        $discografia=$this->requestAlbums($artistId,$access_token);
                        if($discografia->getStatusCode() == 401){
                            #Solicito y guardo NUEVAMENTE un access_token
                            $access_token = $this->requestAccessToken();
                            if(isset($access_token->error)){
                                return response()->json($access_token->error);
                            }
                            $discografia=$this->requestAlbums($artistId,$access_token);
                        }
                        if($discografia->getStatusCode() == 200){
                            $discografia=json_decode($discografia->getBody());
                        }else{
                            return response()->json([
                                'error' => [
                                    'code' => 'error',
                                    'message' => json_decode($discografia->getBody())
                                ]
                            ], 500); 
                        }

                    # Armo el obj Album
                    foreach($discografia->items as $disco){
                        $album= new Album();
                        $album->name=$disco->name;
                        $album->released=$disco->release_date;
                        $album->tracks=$disco->total_tracks;
                        $album->cover=$disco->images;
                        array_push($albumes,$album);
                    }
                    $coincidencia=true;
                    continue;
                }   
            }
            if($coincidencia){
                return response()->json($albumes);
            }else{
                return response()->json([
                    'error' => [
                        'code' => 'error',
                        'message' => 'No se encontro una coincidencia exacta para el mombre ed la banda ingresado.',
                    ]
                ], 400);
            }
    }

    /**
     * Metodo privado que permite solicitar access_token a Spotify API y guardarlo en la BD
     */
    private function requestAccessToken(){
        $tokenRequest=json_decode($this->spotifyClient->auth()->getBody());
        if(!isset($tokenRequest->access_token)){
            return response()->json([
                'error' => [
                    'code' => 'error',
                    'message' => $tokenRequest,
                ]
            ], 500);  
        }
        $access_token=$tokenRequest->access_token;
        #Guardar en BD el access_token
        Token::create([
            'access_token'=>$access_token
            ]);
        return $access_token;
    }

    /**
     * Metodo privado que permite solicitar lista de artistas que concidan con un nombre a Spotify API
     * @param  string  $artistName: nombre del artista a buscar
     * @param  string  $access_token: access_token de autorizacion
     * @return \Illuminate\Http\Response
     */

    private function requestArtist($artistName,$access_token){
        return $this->spotifyClient->artistByName($artistName,$access_token);
    }

    /**
     * Metodo privado que permite solicitar lista de albumes de un artista a Spotify API
     * @param  string  $artistId: id del artista a buscar
     * @param  string  $access_token: access_token de autorizacion
     * @return \Illuminate\Http\Response
     */
    private function requestAlbums($artistId,$access_token){
        return $this->spotifyClient->getAlbums($artistId,$access_token);
    }

}

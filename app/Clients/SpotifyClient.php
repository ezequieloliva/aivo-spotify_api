<?php

namespace App\Clients;

use GuzzleHttp\Client;


class SpotifyClient
{
   /**
     * Metodoque permite autenticarse contra la API de Spotify
     *
     * @return Response
     */
    public function auth(){
        $client = new Client(['http_errors' => true]);
        $response = $client->request(
            'POST',
            env("SPOTIFY_TOKEN_HOST").env("SPOTIFY_TOKEN_PATH"),
            [
                "headers" =>[
                    "Authorization" => "Basic ".env("SPOTIFY_CREDENTIALS")
                ],
                'form_params' => [
                    'grant_type' => env("SPOTIFY_GRANT_TYPE")
                ]
            ]);
        return $response;
    }

    /**
     * Metodoque permite buscar el ID de un artista por su Nombre
     *
     * @return Response
     */
    public function artistByName($name,$access_token){
        $client = new Client(['http_errors' => false]);
        $response = $client->request(
            'GET',
            env('SPOTIFY_API_HOST').env('SPOTIFY_SEARCH_PATH').$name.'&type=artist',
            [
                "headers" =>[
                    "Authorization" => "Bearer ".$access_token
                ]
            ]);
            
        return $response;
    }

    /**
     * Metodoque permite buscar la discografia de un artista
     *
     * @return Response
     */
    public function getAlbums($artistId,$access_token){
        $client = new Client(['http_errors' => false]);
        $response = $client->request(
            'GET',
            env('SPOTIFY_API_HOST').env('SPOTIFY_ARTIST_PATH').$artistId.'/albums',
            [
                "headers" =>[
                    "Authorization" => "Bearer ".$access_token
                ]
            ]);
            
        return $response;
    }

}

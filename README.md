# Aspectos generales de implementación.

- Se creó con PHP Lumen (micro-framework de Laravel) un servicio Rest que permite, mediante un endpoint, obtener la discografía de un artista.
- El servicio REST se implementó en localhost.
- El servicio REST se expone con el context /api/v1.
- El servicio REST expone un recurso GET /albums?q={artista} el cual permite, por medio de query params ingresar el nombre del artista.
- Se implementaron UnitTest para validar el funcionamiento del servicio.


# Detalle de la solución.

- Entra una solicitud a la API AIVO Desafío.
- El servicio:
- - busca el último access_token guardado. 
- - si el último access_token guardado expiró, solicita un nuevo access_token a la API de Spotify y lo guarda en la BD.
- El servicio realiza las peticiones a la API de Spotify para buscar el id del artista y traer su discografía.
- El servicio arma la respuesta y la muestra al cliente.

# Como probar

- Verificar que tengamos Apache y MySql corriendo.
- Crear la BD cn la tabla tal cual figura en el archivo query.sql
- Verificar que tengamos PHP 8
- Verificar que tengamos composer instalado
- Copiar el contenido del archivo .env.example a .env
- Correr el comando composer install
- Levantar servicio: ubicados en el root del proyecto correr php -S localhost:8000 -t public  (en el puerto que tengamos disponible)
- Correr Unit Test: En windows, ubicados en el root del proyecto,  correr: php vendor/phpunit/phpunit/phpunit
- Probar desde Postman.
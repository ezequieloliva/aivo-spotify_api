<?php
use Illuminate\Http\Response;

class AlbumControllerTest extends TestCase
{
    /**
     * Metodo que prueba getAlbums.
     *
     * @test
     */
    public function test_getAlbums()
    {
        $response = $this->call('GET', '/api/v1/albums?q=queen');
        $this->assertEquals(200, $response->status());
        $this->seeJsonStructure([
            [
                'name',
                'released',
                'tracks',
                'cover'
            ]
        ]);
    }
}